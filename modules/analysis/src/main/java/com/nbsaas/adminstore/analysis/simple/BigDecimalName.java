package com.nbsaas.adminstore.analysis.simple;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BigDecimalName {

    private BigDecimal num;

    private String label;
}
