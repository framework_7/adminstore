package com.nbsaas.adminstore.analysis.request;

import lombok.Data;

import java.util.Date;

@Data
public class OrderNumRequest  {

    private Date beginDate;

    private Date endDate;

}
