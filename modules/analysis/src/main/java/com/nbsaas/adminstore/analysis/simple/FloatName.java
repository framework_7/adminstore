package com.nbsaas.adminstore.analysis.simple;

import lombok.Data;

@Data
public class FloatName {

    private String label;

    private Float num;
}
