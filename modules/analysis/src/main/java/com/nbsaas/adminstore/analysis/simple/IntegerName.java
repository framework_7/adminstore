package com.nbsaas.adminstore.analysis.simple;

import lombok.Data;

@Data
public class IntegerName {

    private Integer num;

    private String label;
}
