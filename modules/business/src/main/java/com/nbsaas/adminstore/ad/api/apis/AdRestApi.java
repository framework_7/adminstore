package com.nbsaas.adminstore.ad.api.apis;

import com.haoxuer.discover.rest.api.BaseApi;
import com.nbsaas.adminstore.ad.api.domain.request.AdDataRequest;
import com.nbsaas.adminstore.ad.api.domain.request.AdSearch;
import com.nbsaas.adminstore.ad.api.domain.response.AdResponse;
import com.nbsaas.adminstore.ad.api.domain.simple.AdSimple;

public interface AdRestApi extends BaseApi<AdResponse, AdSimple, AdDataRequest, AdSearch> {
}
