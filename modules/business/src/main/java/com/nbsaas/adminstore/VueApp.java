package com.nbsaas.adminstore;

import com.nbsaas.adminstore.customer.data.entity.Customer;
import com.nbsaas.adminstore.oa.data.entity.WorkLogger;
import com.nbsaas.codemake.CodeMake;
import com.nbsaas.codemake.template.hibernateSimple.TemplateHibernateSimpleDir;
import com.nbsaas.codemake.templates.elementuiForm.ElementUIFormDir;
import com.nbsaas.codemake.templates.vue.VueDir;

import java.io.File;

/**
 * Hello world!
 */
public class VueApp {
    public static void main(String[] args) {

        code().makes(WorkLogger.class);
    }

    private static CodeMake code() {
        CodeMake make = new CodeMake(VueDir.class, TemplateHibernateSimpleDir.class);
        File view = new File("E:\\codes\\vue\\adminstore\\src\\views");
        make.setView(view);
        make.setView(true);
        make.setCodeStyle("vue");
        return make;
    }

}
