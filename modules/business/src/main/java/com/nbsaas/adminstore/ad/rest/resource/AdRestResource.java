package com.nbsaas.adminstore.ad.rest.resource;

import com.haoxuer.discover.data.core.BaseRestDao;
import com.nbsaas.adminstore.ad.api.apis.AdRestApi;
import com.nbsaas.adminstore.ad.api.domain.request.AdDataRequest;
import com.nbsaas.adminstore.ad.api.domain.request.AdSearch;
import com.nbsaas.adminstore.ad.api.domain.response.AdResponse;
import com.nbsaas.adminstore.ad.api.domain.simple.AdSimple;
import com.nbsaas.adminstore.ad.data.entity.Ad;
import com.nbsaas.adminstore.ad.rest.convert.AdFormConvert;
import com.nbsaas.adminstore.ad.rest.convert.AdResponseConver;
import com.nbsaas.adminstore.ad.rest.convert.AdSimpleConver;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class AdRestResource extends BaseRestDao<Ad, AdResponse, AdSimple, AdDataRequest, AdSearch> implements AdRestApi {
    @Override
    protected Class<Ad> getEntityClass() {
        return Ad.class;
    }

    @Override
    public Function<Ad, AdSimple> getConvertSimple() {
        return new AdSimpleConver();
    }

    @Override
    public Function<AdDataRequest, Ad> getConvertForm() {
        return new AdFormConvert();
    }

    @Override
    public Function<Ad, AdResponse> getConvertResponse() {
        return new AdResponseConver();
    }
}
