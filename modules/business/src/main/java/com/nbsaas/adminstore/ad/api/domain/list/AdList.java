package com.nbsaas.adminstore.ad.api.domain.list;


import com.nbsaas.adminstore.ad.api.domain.simple.AdSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2020年11月06日23:28:28.
*/

@Data
public class AdList  extends ResponseList<AdSimple> {

}