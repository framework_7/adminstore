package com.nbsaas.adminstore.shop.api.domain.page;


import com.nbsaas.adminstore.shop.api.domain.simple.ShopSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年01月23日16:17:59.
*/

@Data
public class ShopPage  extends ResponsePage<ShopSimple> {

}