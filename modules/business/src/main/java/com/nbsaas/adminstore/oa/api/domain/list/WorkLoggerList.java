package com.nbsaas.adminstore.oa.api.domain.list;


import com.nbsaas.adminstore.oa.api.domain.simple.WorkLoggerSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年05月22日09:52:36.
*/

@Data
public class WorkLoggerList  extends ResponseList<WorkLoggerSimple> {

}