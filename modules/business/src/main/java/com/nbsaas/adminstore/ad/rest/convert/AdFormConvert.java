package com.nbsaas.adminstore.ad.rest.convert;

import com.nbsaas.adminstore.ad.api.domain.request.AdDataRequest;
import com.nbsaas.adminstore.ad.data.entity.Ad;
import org.springframework.beans.BeanUtils;

import java.util.function.Function;

public class AdFormConvert implements Function<AdDataRequest, Ad> {
    @Override
    public Ad apply(AdDataRequest request) {
        Ad result=new Ad();
        BeanUtils.copyProperties(request,result);
        return result;
    }
}
