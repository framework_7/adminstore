package com.nbsaas.adminstore.customer.api.domain.page;


import com.nbsaas.adminstore.customer.api.domain.simple.CustomerSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年05月23日15:15:47.
*/

@Data
public class CustomerPage  extends ResponsePage<CustomerSimple> {

}