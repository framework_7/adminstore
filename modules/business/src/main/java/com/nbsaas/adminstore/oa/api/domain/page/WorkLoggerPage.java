package com.nbsaas.adminstore.oa.api.domain.page;


import com.nbsaas.adminstore.oa.api.domain.simple.WorkLoggerSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年05月22日09:52:36.
*/

@Data
public class WorkLoggerPage  extends ResponsePage<WorkLoggerSimple> {

}