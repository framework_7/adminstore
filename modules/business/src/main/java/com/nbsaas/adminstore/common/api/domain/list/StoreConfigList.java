package com.nbsaas.adminstore.common.api.domain.list;


import com.nbsaas.adminstore.common.api.domain.simple.StoreConfigSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年04月24日18:10:25.
*/

@Data
public class StoreConfigList  extends ResponseList<StoreConfigSimple> {

}