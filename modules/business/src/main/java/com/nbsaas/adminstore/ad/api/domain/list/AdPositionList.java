package com.nbsaas.adminstore.ad.api.domain.list;


import com.nbsaas.adminstore.ad.api.domain.simple.AdPositionSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2020年11月06日23:22:43.
*/

@Data
public class AdPositionList  extends ResponseList<AdPositionSimple> {

}