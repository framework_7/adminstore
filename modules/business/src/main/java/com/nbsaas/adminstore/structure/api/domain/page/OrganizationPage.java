package com.nbsaas.adminstore.structure.api.domain.page;


import com.nbsaas.adminstore.structure.api.domain.simple.OrganizationSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年04月12日20:37:02.
*/

@Data
public class OrganizationPage  extends ResponsePage<OrganizationSimple> {

}