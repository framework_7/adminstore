package com.nbsaas.adminstore.structure.api.domain.list;


import com.nbsaas.adminstore.structure.api.domain.simple.OrganizationSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年04月12日20:37:02.
*/

@Data
public class OrganizationList  extends ResponseList<OrganizationSimple> {

}