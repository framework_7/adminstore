package com.nbsaas.adminstore.article.api.domain.page;


import com.nbsaas.adminstore.article.api.domain.simple.ArticleCatalogSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2020年11月04日20:16:48.
*/

@Data
public class ArticleCatalogPage  extends ResponsePage<ArticleCatalogSimple> {

}