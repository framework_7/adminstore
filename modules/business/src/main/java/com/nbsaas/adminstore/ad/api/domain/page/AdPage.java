package com.nbsaas.adminstore.ad.api.domain.page;


import com.nbsaas.adminstore.ad.api.domain.simple.AdSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2020年11月06日23:28:28.
*/

@Data
public class AdPage  extends ResponsePage<AdSimple> {

}