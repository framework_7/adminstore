package com.nbsaas.adminstore.common.api.domain.page;


import com.nbsaas.adminstore.common.api.domain.simple.StoreConfigSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年04月24日18:10:25.
*/

@Data
public class StoreConfigPage  extends ResponsePage<StoreConfigSimple> {

}