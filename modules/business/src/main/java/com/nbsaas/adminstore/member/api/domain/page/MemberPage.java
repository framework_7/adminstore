package com.nbsaas.adminstore.member.api.domain.page;


import com.nbsaas.adminstore.member.api.domain.simple.MemberSimple;
import com.haoxuer.discover.rest.base.ResponsePage;
import lombok.Data;

/**
*
* Created by imake on 2021年05月22日10:14:20.
*/

@Data
public class MemberPage  extends ResponsePage<MemberSimple> {

}