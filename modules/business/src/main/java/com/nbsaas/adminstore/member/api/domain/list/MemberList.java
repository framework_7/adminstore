package com.nbsaas.adminstore.member.api.domain.list;


import com.nbsaas.adminstore.member.api.domain.simple.MemberSimple;
import com.haoxuer.discover.rest.base.ResponseList;
import lombok.Data;

/**
*
* Created by imake on 2021年05月22日10:14:20.
*/

@Data
public class MemberList  extends ResponseList<MemberSimple> {

}